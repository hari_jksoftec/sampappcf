<cfcomponent output="false">


    


    <!--- Get userlist --->
    <cffunction name="getUserlist" access="remote" returntype="struct">
        <cfargument name="page" type="numeric" required="yes">
        <cfargument name="pageSize" type="numeric" required="yes">
        <cfargument name="gridsortcolumn" type="string" required="no" default="">
        <cfargument name="gridsortdir" type="string" required="no" default="">

        <!--- Local variables --->
        <cfset var userlist="">

        <!--- Get data --->
        <cfquery name="userlist" datasource="cftest">
        SELECT *
        FROM SampUser
        <cfif ARGUMENTS.gridsortcolumn NEQ ""
            and ARGUMENTS.gridsortdir NEQ "">
            ORDER BY #ARGUMENTS.gridsortcolumn# #ARGUMENTS.gridsortdir#
        </cfif>
        </cfquery>

        <!--- And return it as a grid structure --->
        <cfreturn QueryConvertForGrid(userlist,
                            ARGUMENTS.page,
                            ARGUMENTS.pageSize)>
    </cffunction>


    <!--- Edit userlist --->
    <cffunction name="editUserlist" access="remote">
        <cfargument name="gridaction" type="string" required="yes">
        <cfargument name="gridrow" type="struct" required="yes">
        <cfargument name="gridchanged" type="struct" required="yes">

        <!--- Local variables --->
        <cfset var colname="">
        <cfset var value="">

        <!--- Process gridaction --->
        <cfswitch expression="#ARGUMENTS.gridaction#">
            <!--- Process updates --->
            <cfcase value="U">
                <!--- Get column name and value --->
                <cfset colname=StructKeyList(ARGUMENTS.gridchanged)>
                <cfset value=ARGUMENTS.gridchanged[colname]>
                <!--- Perform actual update --->
                <cfquery datasource="cftest">
                UPDATE SampUser
                SET #colname# = '#value#'
                WHERE id = #ARGUMENTS.gridrow.id#
                </cfquery>
            </cfcase>
            <!--- Process deletes --->
            <cfcase value="D">
                <!--- Perform actual delete --->
                <cfquery datasource="cftest">
                DELETE FROM SampUser
                WHERE id = #ARGUMENTS.gridrow.id#
                </cfquery>
            </cfcase>
        </cfswitch>
    </cffunction>


</cfcomponent>