<cfform>
    <cfgrid name="userlist"
            format="html"
            pagesize="10"
            striperows="yes"
            selectmode="edit"
            bind="cfc:userlist.getUserlist({cfgridpage},
                                        {cfgridpagesize},
                                        {cfgridsortcolumn},
                                        {cfgridsortdirection})"
            onchange="cfc:userlist.editUserlist({cfgridaction},
                                            {cfgridrow},
                                            {cfgridchanged})">
        <cfgridcolumn name="id" display="false" />
        <cfgridcolumn name="name" header="Name" width="100"/>
        <cfgridcolumn name="jobtitle" header="Job Title" width="100"/>
        <cfgridcolumn name="datestarted" header="Date Started" width="200"/>
        <cfgridcolumn name="OfficeLoction" header="Office Location" width="200"/>
    </cfgrid>
</cfform>

